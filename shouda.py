def dfs_aux(key,amap,aset,ret):
    ret.add(key)
    children_list = amap.get(key,[])
    for next in children_list:
        if next not in aset:
            aset.add(next)
            dfs_aux(next,amap,aset,ret)
            aset.remove(next)
    
if __name__ == '__main__':
    inputFilePath = "input2.txt"
    f = open(inputFilePath,'r')
    N = int(f.readline())
    map1 = {}
    map2 = {}
    for i in range(1,N+1):
        tmp = int(f.readline())
        if i != tmp:
            map1[i] = int(tmp)
            map2[tmp] = map2.get(tmp,[])
            map2[tmp].append(i)

    ret = set()
    aset = set()
    dfs_aux(3,map2,aset,ret)
    print ret
    print map1,map2
