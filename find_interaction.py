def find_interaction(head,nextmap):
    p1 = head
    p2 = head
    p1 = nextmap[p1]
    p2 = nextmap[p2]
    p2 = nextmap[p2]
    while (p1 != p2):
        p1 = nextmap[p1]
        p2 = nextmap[p2]
        p2 = nextmap[p2]
    #print p1,p2
    p1 = head
    tail = p2
    while (p1 != p2):
        p1 = nextmap[p1]
        tail = p2
        p2 = nextmap[p2]
    
    #get the circle
    circle_head = p1
    circle_set = set()
    circle_set.add(p1)
    p1 = nextmap[p1]
    while(p1 != circle_head):
        circle_set.add(p1)
        p1 = nextmap[p1]

        
    #print circle_set
    return circle_set
    #return tail,p1,p2

def bar(aset, asetlist):
    for item in asetlist:
        if item == aset:
            return
    asetlist.append(aset)

if __name__ == '__main__':
    inputFilePath = "input.txt"
    f = open(inputFilePath,'r')
    N = int(f.readline())
    visited_set = set()
    circle_list = list()
    map1 = {}
    for i in range(1,N+1):
        tmp = int(f.readline())
        #if i != tmp:
        if True:
            map1[i] = int(tmp)

    for key in map1:
        if not key in visited_set:
            ret = find_interaction(key,map1)
            visited_set.add(key)
            bar(ret,circle_list)
    print circle_list;
    flag = 0
    for item in circle_list:
        if 1 in item:
            flag = 1
            break
    result = len(circle_list) - flag
    print result
